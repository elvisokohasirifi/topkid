<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ordering extends Model
{
    protected $fillable = [
        'product', 'price', 'quantity', 'size'
    ];

    protected $guarded = [];

    public function order(){
        return $this->belongsTo(Order::class);
    }
}
