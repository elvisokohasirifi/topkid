<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];

    public function orderings(){
        return $this->hasMany(Ordering::class)->orderBy('created_at', 'DESC');
    }

    public function payment(){
        return $this->hasMany(Payment::class)->orderBy('created_at', 'DESC');
    }
}
