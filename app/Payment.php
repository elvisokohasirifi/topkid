<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'payment_method', 'amount', 'payment_status'
    ];

    protected $guarded = [];

    public function order(){
        return $this->belongsTo(Order::class);
    }
}
