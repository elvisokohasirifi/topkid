<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        $cart = DB::table('carts')->where('ip', '=', $_SERVER['REMOTE_ADDR'])->count();
        $products = DB::table('products')->where('featured', '=', '1')->inRandomOrder()->limit(8)->get();
        $categories = DB::table('categories')->where('featured', '=', '1')->inRandomOrder()->limit(6)->get();
        return view('home', compact('products', 'categories', 'cart'));
    }
}
