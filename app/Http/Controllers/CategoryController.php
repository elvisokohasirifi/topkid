<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\Category;
use App\Product;

class CategoryController extends Controller
{
    public function index()
    {
        $cart = DB::table('carts')->where('ip', '=', $_SERVER['REMOTE_ADDR'])->count();
    	$products = DB::table('products')->where('featured', '=', '1')->inRandomOrder()->limit(4)->get();
    	$categories = DB::table('categories')->orderBy('name', 'asc')->paginate(20);
        return view('categories', compact('categories', 'products', 'cart'));
        //return view('categories');
    }

    public function category($category)
    {
        $categories = DB::table('categories')->where('featured', '=', '1')->inRandomOrder()->limit(4)->get();
    	$category = DB::table('categories')->where('slug', '=', $category)->first();
    	$products = DB::table('products')->where('categories_id', '=', $category->id)->orderBy('name', 'asc')->paginate(20);
        $cart = DB::table('carts')->where('ip', '=', $_SERVER['REMOTE_ADDR'])->count();
        return view('category', compact('products', 'category', 'categories', 'cart'));
    }
}
