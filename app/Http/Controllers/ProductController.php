<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App\Product;

class ProductController extends Controller
{
    public function index()
    {
    	$categories = DB::table('categories')->where('featured', '=', '1')->inRandomOrder()->limit(4)->get();
    	$products = DB::table('products')->orderBy('name', 'asc')->paginate(20);
        $cart = DB::table('carts')->where('ip', '=', $_SERVER['REMOTE_ADDR'])->count();
        return view('products', compact('products', 'categories', 'cart'));
        //return view('products');
    }

    public function product($product)
    {
    	$product = DB::table('products')->where('slug', '=', $product)->first();
    	$sizes = DB::table('product_sizes')->join('sizes', 'sizes.id', '=', 'product_sizes.sizes_id')->where('products_id', '=', $product->id)->select('sizes_id', 'price', 'name')->get();
    	$products = DB::table('products')->where('featured', '=', '1')->inRandomOrder()->limit(4)->get();
        $cart = DB::table('carts')->where('ip', '=', $_SERVER['REMOTE_ADDR'])->count();
        return view('product', compact('product', 'sizes', 'products', 'cart'));
    }

    public function searchproducts()
    {   
    	$validator = Validator::make(request()->all(), [
            'term' => ['required', 'string'],
        ]);
        if($validator->fails()){
            return response()->json([
                'success' => 0,
                'message' => $validator->errors()
            ], 201);       
        } 
        $products = DB::table('products')->where('tags', 'like', '%'.request('term').'%')->orderBy('name', 'asc')->get();
        return response()->json(['success' => 1, 'message' => $products]);     
    }
}
