<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ContactController extends Controller
{
    public function index()
    {
    	$cart = DB::table('carts')->where('ip', '=', $_SERVER['REMOTE_ADDR'])->count();
        return view('contact', compact('cart'));
    }
}
