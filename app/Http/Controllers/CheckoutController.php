<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\Order;
use App\Ordering;
use App\Payment;
use App\Product;
use App\Size;
use Auth;

class CheckoutController extends Controller
{
    public function index()
    {
    	$cart = DB::table('carts')->where('ip', '=', $_SERVER['REMOTE_ADDR'])->count();
        $totalcost = DB::table('carts')->select(DB::raw('sum(price * quantity) as total'))->where('ip', '=', $_SERVER['REMOTE_ADDR'])->get();
        //dd($totalcost);
        $totalcost = json_decode($totalcost, true);
        $totalcost = $totalcost[0]['total'];

        $settings = DB::table('settings')->get();
        //dd($settings);
        $settings = json_decode($settings, true);
        //dd($settings);
        $deliverytype = $settings[0]['deliverytype'];
        $deliverycost = 0;

        $commissiontype = $settings[0]['commissiontype'];
        $commission = 0;

        if ($deliverytype == 0) {
            $deliverycost = $settings[0]['delivery'];
        }
        else{
            $deliverycost = ($settings[0]['delivery'] * $totalcost)/100;
        }

        if ($commissiontype == 0) {
            $commission = $settings[0]['commission'];
        }
        else{
            $commission = ($settings[0]['commission'] * $totalcost)/100;
        }
        return view('checkout', compact('cart', 'deliverycost', 'totalcost', 'commission'));
    }

    public function completeorder(){
        $validator = Validator::make(request()->all(), [
            'name' => ['required', 'string'],
            'country' => ['required', 'string'],
            'street' => ['required', 'string'],
            'apartment' => '',
            'town' => ['required', 'string'],
            'phone' => ['required', 'string'],
            'email' => ['required', 'email'],
            'payment' => ['required', 'string'],
            'remember' => ['required', 'string'],
            'order_no' => ['required', 'numeric'],
            'amount' => ['required', 'numeric']
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => 0,
                'message' => $validator->errors()
            ], 201);       
        } 

        if (Auth::check() && request('remember') == true) {
            $user = auth()->user();
            $user->country = request('country');
            $user->street = request('street');
            $user->apartment = request('apartment');
            $user->town = request('town');
            $user->phone = request('phone');
            $user->save();
        }

        $order = new Order;
        $order->customer = request('name');
        $order->order_no = request('order_no');
        $order->country = request('country');
        $order->street = request('street');
        $order->apartment = request('apartment');
        $order->town = request('town');
        $order->phone = request('phone');
        $order->email = request('email');
        $order->order_status = 'pending';
        $order->save();

        $orderid = Order::findOrFail($order->id);

        $payment = new Payment;
        $order->payment()->create([
            'amount' => request('amount'),
            'payment_method' => request('payment'),
            'payment_status' => 'pending'
        ]);

        //get cart items
        
        $cart = DB::table('carts')->where('ip', '=', $_SERVER['REMOTE_ADDR'])->get();
        $cart = json_decode($cart, true);

        for ($i=0; $i < count($cart); $i++) { 
            if ($cart[$i]['sizes_id'] == NULL) {
                $order->orderings()->create([
                    'product' => Product::findOrFail($cart[$i]['products_id'])->name,
                    'price' => Product::findOrFail($cart[$i]['products_id'])->price,
                    'quantity' => $cart[$i]['quantity'],
                    'size' =>  ''
                ]);
            }
            else{
                $order->orderings()->create([
                    'product' => Product::findOrFail($cart[$i]['products_id'])->name,
                    'price' => Product::findOrFail($cart[$i]['products_id'])->price,
                    'quantity' => $cart[$i]['quantity'],
                    'size' =>  Size::findOrFail($cart[$i]['sizes_id'])->name
                ]);
            }
        }

        DB::table('carts')->where('ip', '=', $_SERVER['REMOTE_ADDR'])->delete();

        return response()->json(['success' => 1]);  
    }
}
