<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\Product;
use App\Cart;

class CartController extends Controller
{
    public function index()
    {
        $cart = DB::table('carts')->where('ip', '=', $_SERVER['REMOTE_ADDR'])->count();
        $carts = DB::table('carts')->select('products_id', 'image', 'products.name as name', 'carts.price as price', 'sizes.name as sizename', 'quantity', 'carts.id as cart')->join('products', 'products.id', '=', 'carts.products_id')->leftJoin('sizes', 'sizes.id', '=', 'carts.sizes_id')->where('ip', '=', $_SERVER['REMOTE_ADDR'])->orderBy('name', 'asc')->get();
        $totalcost = DB::table('carts')->select(DB::raw('sum(price * quantity) as total'))->where('ip', '=', $_SERVER['REMOTE_ADDR'])->get();
        //dd($totalcost);
        $totalcost = json_decode($totalcost, true);
        $totalcost = $totalcost[0]['total'];

        $settings = DB::table('settings')->get();
        //dd($settings);
        $settings = json_decode($settings, true);
        //dd($settings);
        $deliverytype = $settings[0]['deliverytype'];
        $deliverycost = 0;

        $commissiontype = $settings[0]['commissiontype'];
        $commission = 0;

        if ($deliverytype == 0) {
            $deliverycost = $settings[0]['delivery'];
        }
        else{
            $deliverycost = ($settings[0]['delivery'] * $totalcost)/100;
        }

        if ($commissiontype == 0) {
            $commission = $settings[0]['commission'];
        }
        else{
            $commission = ($settings[0]['commission'] * $totalcost)/100;
        }
        return view('cart', compact('cart', 'carts', 'deliverycost', 'totalcost', 'commission'));
    }

    public function add0(){
    	$validator = Validator::make(request()->all(), [
            'id' => ['required', 'numeric'],
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => 0,
                'message' => $validator->errors()
            ], 201);       
        }  
        $product = Product::findOrFail(request('id'));
        $cart = new Cart;
        $cart->ip = request()->ip();
        $cart->products_id = request('id');
        $cart->quantity = 1;
        $cart->price = $product->price;
        $cart->save();
        $cart = DB::table('carts')->where('ip', '=', $_SERVER['REMOTE_ADDR'])->count();
        return response()->json(['success' => 1, 'message' => $cart]); 
    }

    public function add1(){
    	$validator = Validator::make(request()->all(), [
            'id' => ['required', 'numeric'],
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => 0,
                'message' => $validator->errors()
            ], 201);       
        }  
        $product = Product::findOrFail(request('id'));
        $sizes = DB::table('product_sizes')->join('sizes', 'sizes.id', '=', 'product_sizes.sizes_id')->where('products_id', '=', $product->id)->select('sizes_id', 'price', 'name')->get();
        return response()->json(['success' => 1, 'message' => $sizes, 'product' => $product]); 
    }

    public function add2(){
    	$validator = Validator::make(request()->all(), [
            'id' => ['required', 'numeric'],
            'size' => ['required', 'numeric'],
            'quantity' => ['required', 'numeric'],
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => 0,
                'message' => $validator->errors()
            ], 201);       
        }  


        $product = Product::findOrFail(request('id'));

        $cart = new Cart;
        $cart->ip = request()->ip();
        $cart->products_id = request('id');
        $cart->quantity = request('quantity');
        $cart->sizes_id = request('size');
        $cart->price = DB::table('product_sizes')->join('sizes', 'sizes.id', '=', 'product_sizes.sizes_id')->where('products_id', '=', $product->id)->where('sizes_id', '=', request('size'))->select('price')->first()->price;
        $cart->save();
        $cart = DB::table('carts')->where('ip', '=', $_SERVER['REMOTE_ADDR'])->count();
        return response()->json(['success' => 1, 'message' => $cart]); 
    }

    public function deletecart(){
        $validator = Validator::make(request()->all(), [
            'id' => ['required', 'numeric'],
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => 0,
                'message' => $validator->errors()
            ], 201);       
        } 

        DB::table('carts')->where('products_id', '=', request('id'))->delete();
        return response()->json(['success' => 1]); 
    }

    function updatecart(){
        $validator = Validator::make(request()->all(), [
            'cart' => ['required'],
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => 0,
                'message' => $validator->errors()
            ], 201);       
        }  
        $items = request('cart');
        for ($i=0; $i < count($items); $i++) { 
            $cart = Cart::findOrFail($items[$i]['product']);
            $cart->quantity = $items[$i]['qty'];
            $cart->save();
        }
        $totalcost = DB::table('carts')->select(DB::raw('sum(price * quantity) as total'))->where('ip', '=', $_SERVER['REMOTE_ADDR'])->get();
        //dd($totalcost);
        $totalcost = json_decode($totalcost, true);
        $totalcost = $totalcost[0]['total'];

        $settings = DB::table('settings')->get();
        //dd($settings);
        $settings = json_decode($settings, true);
        //dd($settings);
        $deliverytype = $settings[0]['deliverytype'];
        $deliverycost = 0;

        $commissiontype = $settings[0]['commissiontype'];
        $commission = 0;

        if ($deliverytype == 0) {
            $deliverycost = $settings[0]['delivery'];
        }
        else{
            $deliverycost = ($settings[0]['delivery'] * $totalcost)/100;
        }

        if ($commissiontype == 0) {
            $commission = $settings[0]['commission'];
        }
        else{
            $commission = ($settings[0]['commission'] * $totalcost)/100;
        }
        return response()->json(['success' => 1, 'total' => $totalcost, 'commission' => $commission, 'delivery' => $deliverycost]); 
    }
}
