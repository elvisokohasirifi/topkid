function searchproducts(term){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.post("/searchproducts", {id: id},
        function(data, status){
          let ans = data['message'];
          let total = 0;
          let tab = "<table class='table table-striped'><tr><th>Product</th><th>Price</th><th>Quantity</th><th>Total</th></tr>";
          for(var i = 0; i < ans.length; i++){
              tab += '<tr>';
              tab += ('<td>' + ans[i]['product'] + "</td>");
              tab += ('<td>' + ans[i]['price'] + "</td>");
              tab += ('<td>' + ans[i]['quantity'] + "</td>");
              tab += ('<td>' + ans[i]['quantity'] * ans[i]['price'] + "</td>");
              total += (ans[i]['quantity'] * ans[i]['price']);
              tab += '</tr>';
          }
          tab += "<tr><td><b>Total</b></td><td></td><td></td><td><b>$" + total + "</b></td></tr></table>";
          $('#viewmodal').modal('show'); 
        document.getElementById('orderbox').innerHTML = tab;
        document.getElementById('orderno').innerHTML = orderno;          
    });
    
    //document.getElementById('id').innerHTML = id;
 }

 function changeprice(arr){
    var val = document.getElementById('productprice').value;
    var index = arr.findIndex(x => x.sizes_id == val);
    document.getElementById('sizeprice').innerHTML = arr[index]['price'];
 }

 function addtocart(id, hassizes){
   $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    if (hassizes == 0) { 
      $.post("/addtocart0", {id: id},
          function(data, status){
            alert('added to cart'); 
            document.getElementById('cartnumber').innerHTML = data['message'];  
      });
    }
    else{
      $.post("/addtocart1", {id: id},
          function(data, status){  
          console.log(data);          
            document.getElementById('exampleModalLongTitle').innerHTML = data['product']['name'];
            document.getElementById('id').value = data['product']['id'];
            $('#viewmodal').modal('show'); 
            for (var i = data['message'].length - 1; i >= 0; i--) {
              document.getElementById('size').innerHTML += ("<option value='" + data['message'][i]['sizes_id'] + "'>" + data['message'][i]['name'] + "</option>");
            }
      });
    }
 }

 function addtothecart(){
   $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
     let id = document.getElementById('id').value;
     let qty = document.getElementById('qty').value;
     let size = document.getElementById('size').value;
     $('#viewmodal').modal('hide'); 
    $.post("/addtocart2", {id: id, size: size, quantity: qty},
        function(data, status){ 
        if (data['success'] == 1) {           
          alert('added to cart');
          document.getElementById('cartnumber').innerHTML = data['message'];
          //$('#viewmodal').modal('close'); 
        }
        else{
            console.log(data);
        }
    });
  }

 function deletecart(id){
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.post("/deletecart", {id: id},
        function(data, status){ 
        if (data['success'] == 1) {           
          window.location.href = "/cart";
        }
        else{
            console.log(data);
        }
    });
 }

 function updatevalues(){
     document.getElementById('body').style.opacity = '0.5';
     var table = document.getElementById("carttable");
     var x = table.rows.length;
     var cart = [];
     for(var i = 1; i < x; i++){
        var product = table.rows[i].cells[2].getElementsByTagName('input')[0].value;
        var qty = table.rows[i].cells[5].getElementsByTagName('input')[0].value;
        cart.push({product:product, qty:qty});
     }
     console.log(cart);

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.post("/updatecart", {cart: cart},
        function(data, status){ 
        if (data['success'] == 1) {    
          document.getElementById('subtotal').innerHTML = 'GHS ' + data['total'];
          document.getElementById('delivery').innerHTML = 'GHS ' + data['delivery'];
          document.getElementById('commission').innerHTML = 'GHS ' + data['commission'];
          document.getElementById('total').innerHTML = 'GHS ' + (data['total'] + data['delivery'] + data['commission']);
        }
        else{
            console.log(data);
        }
        document.getElementById('body').style.opacity = '1';
    });
 }

 function newtotal(price, id, quantity){
     if (quantity != null && !isNaN(quantity))
         document.getElementById("total"+id).innerHTML = price * quantity;
 }

 function login(){
     $('#loginmodal').modal('show'); 
 }

 function signup(){
     $('#signupmodal').modal('show'); 
 }

 function completeorder(){
   let customername = document.getElementById('customername').value;
   let customercountry = document.getElementById('customercountry').value;
   let customerstreet = document.getElementById('customerstreet').value;
   let customerapartment = document.getElementById('customerapartment').value;
   let customertown = document.getElementById('customertown').value;
   let customerphone = document.getElementById('customerphone').value;
   let customeremail = document.getElementById('customeremail').value;
   let customerpayment = document.getElementById('customerpayment').value;
   let customerremember = document.getElementById('customerremember').checked;
   let customeramount = document.getElementById('customeramount').value;

   saveorder(customername, customercountry, customerstreet, customerapartment, customertown, 
     customerphone, customeremail, customerpayment, customerremember, customeramount);
 }

 function saveorder(n, c, s, a, t, p, e, pay, r, amount){
   document.getElementById('body').style.opacity = '0.5';
   $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var dates = Date.now();
    $.post("/completeorder", {name:n, country:c, street:s, apartment:a, town:t, phone:p, email:e, payment:pay, remember:r, order_no: dates, amount: amount},
        function(data, status){ 
        if (data['success'] == 1) {  
          document.getElementById('body').style.opacity = '1';  
          payWithRave(e, amount, p, n, dates);
        }
        else{
          document.getElementById('body').style.opacity = '1';
          console.log(data);
          let s = "<p>";
          let values = data['message'];
          for (var key in values) {
            var value = values[key];
            s = s + value + "<br>";
          }
          s += '</p>';
                    Swal.fire(
            'Oops!',
            s,
            'error'
          )
        }
    });
 }

 function payWithRave(e, amount, p, n, d) {
   const API_publicKey = "FLWPUBK_TEST-55989376ec82d8fde1b8280325632463-X";
    var x = getpaidSetup({
        PBFPubKey: API_publicKey,
        customer_email: e,
        amount: amount,
        customer_phone: p,
        customer_firstname: n,
        currency: "NGN",
        txref: d,
        meta: [{
            metaname: "flightID",
            metavalue: "AP1234"
        }],
        onclose: function() {},
        callback: function(response) {
            var txref = response.tx.txRef; // collect txRef returned and pass to a                  server page to complete status check.
            //console.log("This is the response returned after a charge", response);
            if (
                response.tx.chargeResponseCode == "00" ||
                response.tx.chargeResponseCode == "0"
            ) {
                //alert('success');
                success(d, amount);
            } else {
                alert(response.data.responsemessage);
            }

            x.close(); // use this to close the modal immediately after payment.
        }
    });
}

function success(dates, amount){
  $.ajaxSetup({           
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  
  $.post("/addpayment", {date: dates, amount: amount},
    function(data, status){
      if (data == 1) {
        alert('success');
        window.location.href = '/dues';
      }
    });
}