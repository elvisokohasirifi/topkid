<!DOCTYPE html>
<html lang="en">
<head>
	<title>BuyGiveMe</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="vegefoods/css/open-iconic-bootstrap.min.css">
	<link rel="stylesheet" href="vegefoods/css/animate.css">

	<link rel="stylesheet" href="vegefoods/css/owl.carousel.min.css">
	<link rel="stylesheet" href="vegefoods/css/owl.theme.default.min.css">
	<link rel="stylesheet" href="vegefoods/css/magnific-popup.css">

	<link rel="stylesheet" href="vegefoods/css/aos.css">

	<link rel="stylesheet" href="vegefoods/css/ionicons.min.css">

	<link rel="stylesheet" href="vegefoods/css/bootstrap-datepicker.css">
	<link rel="stylesheet" href="vegefoods/css/jquery.timepicker.css">

	<link rel="stylesheet" href="vegefoods/css/flaticon.css">
	<link rel="stylesheet" href="vegefoods/css/icomoon.css">
	<link rel="stylesheet" href="vegefoods/css/style.css">
</head>
<body class="goto-here">

	<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
		<div class="container">
			<a class="navbar-brand" href="/home">BuyGiveMe</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="oi oi-menu"></span> Menu
			</button>

			<div class="collapse navbar-collapse" id="ftco-nav">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item"><a href="/home" class="nav-link">Home</a></li>
					<li class="nav-item"><a href="/products" class="nav-link">Products</a></li>
					<li class="nav-item"><a href="/categories" class="nav-link">Categories</a></li>
					<li class="nav-item"><a href="/checkout" class="nav-link">Checkout</a></li>
					<li class="nav-item"><a href="/about" class="nav-link">About</a></li>
					<li class="nav-item"><a href="/contact" class="nav-link">Contact</a></li>
					<li class="nav-item cta cta-colored"><a href="/cart" class="nav-link"><span class="icon-shopping_cart"></span>[<span id="cartnumber">{{ $cart }}</span>]</a></li>

				</ul>
			</div>
		</div>
	</nav>
	<!-- END nav -->

	<section id="home-section" class="hero">
		<div class="home-slider owl-carousel">
			<div class="slider-item" style="background-image: url(vegefoods/images/bg-3.jpg);">
				<div class="overlay"></div>
				<div class="container">
					<div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

						<div class="col-md-12 ftco-animate text-center">
							<h1 class="mb-2">We deliver freshness</h1>
							<h2 class="subheading mb-4">We deliver fresh vegetables &amp; fruits</h2>
							<p><a href="#" class="btn btn-primary">View Details</a></p>
						</div>

					</div>
				</div>
			</div>

			<div class="slider-item" style="background-image: url(vegefoods/images/bg-1.jpg);">
				<div class="overlay"></div>
				<div class="container">
					<div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

						<div class="col-sm-12 ftco-animate text-center">
							<h1 class="mb-2">Straight from the market</h1>
							<h2 class="subheading mb-4">We deliver from your choice market</h2>
							<p><a href="#" class="btn btn-primary">View Details</a></p>
						</div>

					</div>
				</div>
			</div>

			<div class="slider-item" style="background-image: url(vegefoods/images/bg_1.jpg);">
				<div class="overlay"></div>
				<div class="container">
					<div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

						<div class="col-sm-12 ftco-animate text-center">
							<h1 class="mb-2">To your door step</h1>
							<h2 class="subheading mb-4">We deliver to your door step</h2>
							<p><a href="#" class="btn btn-primary">View Details</a></p>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="ftco-section">
		<div class="container">
			<div class="row no-gutters ftco-services">
				<div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
					<div class="media block-6 services mb-md-0 mb-4">
						<div class="icon bg-color-4 d-flex justify-content-center align-items-center mb-2">
							<span class="flaticon-customer-service"></span>
						</div>
						<div class="media-body">
							<h3 class="heading">You Order</h3>
							<span>At your convenience</span>
						</div>
					</div>      
				</div>
				<div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
					<div class="media block-6 services mb-md-0 mb-4">
						<div class="icon bg-color-2 d-flex justify-content-center align-items-center mb-2">
							<span class="flaticon-diet"></span>
						</div>
						<div class="media-body">
							<h3 class="heading">We Purchase</h3>
							<span>From trusted sources</span>
						</div>
					</div>    
				</div>
				<div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
					<div class="media block-6 services mb-md-0 mb-4">
						<div class="icon bg-color-3 d-flex justify-content-center align-items-center mb-2">
							<span class="flaticon-award"></span>
						</div>
						<div class="media-body">
							<h3 class="heading">We Package</h3>
							<span>With safe materials</span>
						</div>
					</div>      
				</div>
				<div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
					<div class="media block-6 services mb-md-0 mb-4">
						<div class="icon bg-color-1 active d-flex justify-content-center align-items-center mb-2">
							<span class="flaticon-shipped"></span>
						</div>
						<div class="media-body">
							<h3 class="heading">We deliver</h3>
							<span>to your doorstep</span>
						</div>
					</div>      
				</div>

			</div>
		</div>
	</section>

	<section class="ftco-section ftco-category ftco-no-pt">
		<div class="container">
			<div class="row justify-content-center mb-3 pb-3">
				<div class="col-md-12 heading-section text-center ftco-animate">
					<span class="subheading">Featured Categories</span>
					<h2 class="mb-4">Most Sought Categories</h2>
				</div>
			</div>   		
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						@foreach($categories as $category)
						<div class="col-md-6 col-lg-4 ftco-animate">
				            <div class="category-wrap ftco-animate img mb-4 d-flex align-items-end" style="background-image: url(vegefoods/images/{{ $category->image }});">
				              <div class="text px-3 py-1">
				                <h2 class="mb-0"><a href="/category/{{ $category->slug }}">{{ $category->name }}</a></h2>
				              </div>
				            </div>
				        </div>
				        @endforeach
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center mb-3 pb-3">
				<div class="col-md-12 heading-section text-center ftco-animate">
					<span class="subheading">Featured Products</span>
					<h2 class="mb-4">Most Purchased Items</h2>
				</div>
			</div>   		
		</div>
		<div class="container">
			<div class="row">
				@foreach($products as $product)
				<div class="col-md-6 col-lg-3 ftco-animate">
					<div class="product">
						<a href="/product/{{ $product->slug }}" class="img-prod"><img class="img-fluid" src="vegefoods/images/{{ $product->image }}" alt="{{ $product->name }}">
							<div class="overlay"></div>
						</a>
						<div class="text py-3 pb-4 px-3 text-center">
							<h3><a href="/product/{{ $product->slug }}">{{ $product->name }}</a></h3>
							<div class="d-flex">
								<div class="pricing">
									<p class="price"><span>GHS {{ $product->price }}</span></p>
								</div>
							</div>
							<div class="d-flex justify-content-center">
								<div class="text-center">
									<p class="price"><button class="btn btn-primary" onclick="addtocart({{ $product->id }}, {{ $product->has_sizes }})">Add to Cart</button></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</section>

	<section class="ftco-section img" style="background-image: url(vegefoods/images/bg_3.jpg);">
		<div class="container">
			<div class="row justify-content-end">
				<div class="col-md-6 heading-section ftco-animate deal-of-the-day ftco-animate">
					<span class="subheading">Best Price For You</span>
					<h2 class="mb-4">Deal of the day</h2>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
					<h3><a href="#">Spinach</a></h3>
					<span class="price">$10 <a href="#">now $5 only</a></span>
					<div id="timer" class="d-flex mt-5">
						<div class="time" id="days"></div>
						<div class="time pl-3" id="hours"></div>
						<div class="time pl-3" id="minutes"></div>
						<div class="time pl-3" id="seconds"></div>
					</div>
				</div>
			</div>   		
		</div>
	</section>

  <section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
  	<div class="container py-4">
  		<div class="row d-flex justify-content-center py-5">
  			<div class="col-md-6">
  				<h2 style="font-size: 22px;" class="mb-0">Subcribe to our Newsletter</h2>
  				<span>Get e-mail updates about our latest shops and special offers</span>
  			</div>
  			<div class="col-md-6 d-flex align-items-center">
  				<form action="#" class="subscribe-form">
  					<div class="form-group d-flex">
  						<input type="text" class="form-control" placeholder="Enter email address">
  						<input type="submit" value="Subscribe" class="submit px-3">
  					</div>
  				</form>
  			</div>
  		</div>
  	</div>
  </section>
  <footer class="ftco-footer ftco-section">
  	<div class="container">
  		<div class="row">
  			<div class="mouse">
  				<a href="#" class="mouse-icon">
  					<div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
  				</a>
  			</div>
  		</div>
  		<div class="row mb-5">
  			<div class="col-md">
  				<div class="ftco-footer-widget mb-4">
  					<h2 class="ftco-heading-2">BUYGIVEME</h2>
  					<p>We are here to serve everyone. Going to the market is difficult under this hot weather. So order, we will deliver.</p>
  					<ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
  						<li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
  						<li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
  						<li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
  					</ul>
  				</div>
  			</div>
  			<div class="col-md">
  				<div class="ftco-footer-widget mb-4 ml-md-5">
  					<h2 class="ftco-heading-2">Menu</h2>
  					<ul class="list-unstyled">
  						<li><a href="/products" class="py-2 d-block">Shop</a></li>
  						<li><a href="/about" class="py-2 d-block">About</a></li>
  						<li><a href="/categories" class="py-2 d-block">Categories</a></li>
  						<li><a href="/contact" class="py-2 d-block">Contact Us</a></li>
  					</ul>
  				</div>
  			</div>
  			<div class="col-md-4">
  				<div class="ftco-footer-widget mb-4">
  					<h2 class="ftco-heading-2">Help</h2>
  					<div class="d-flex">
  						<ul class="list-unstyled mr-l-5 pr-l-3 mr-4">
  							<li><a href="/shipping" class="py-2 d-block">Shipping Information</a></li>
  							<li><a href="/returns" class="py-2 d-block">Returns &amp; Exchange</a></li>
  							<li><a href="/terms" class="py-2 d-block">Terms &amp; Conditions</a></li>
  							<li><a href="/privacy" class="py-2 d-block">Privacy Policy</a></li>
  						</ul>
  						<ul class="list-unstyled">
  							<li><a href="/faqs" class="py-2 d-block">FAQs</a></li>
  							<li><a href="/contact" class="py-2 d-block">Contact</a></li>
  						</ul>
  					</div>
  				</div>
  			</div>
  			<div class="col-md">
  				<div class="ftco-footer-widget mb-4">
  					<h2 class="ftco-heading-2">Have Questions?</h2>
  					<div class="block-23 mb-3">
  						<ul>
  							<li><span class="icon icon-map-marker"></span><span class="text">Fourth Street, Madina, Accra, Ghana</span></li>
  							<li><a href="#"><span class="icon icon-phone"></span><span class="text">+233 (0)20 435 5647</span></a></li>
  							<li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@buygiveme.com</span></a></li>
  						</ul>
  					</div>
  				</div>
  			</div>
  		</div>
  		<div class="row">
  			<div class="col-md-12 text-center">

  				<p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  					Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart color-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  				</p>
  			</div>
  		</div>
  	</div>
  </footer>

  <div class="modal fade" id="viewmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Order #<span id="orderno"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="orderbox"></div>
        <form>
            @csrf
            <input type="hidden" name="id" id="id">
            <div class="form-group">
                <label for="size">Size</label>
                <select name="size" id="size" class="form-control @error('size') is-invalid @enderror" placeholder=""></select> 
            </div>
            <div class="form-group">
                <label for="qty">Quantity</label>
                <input type="number" class="form-control @error('qty') is-invalid @enderror" id="qty" placeholder="" name="qty" value="1">
            </div>
            <hr>
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary btn-lg" onclick="addtothecart()">Add to Cart</button>
        </form>
      </div>
    </div>
  </div>
</div>

  <script src="vegefoods/js/jquery.min.js"></script>
  <script src="vegefoods/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="vegefoods/js/popper.min.js"></script>
  <script src="vegefoods/js/bootstrap.min.js"></script>
  <script src="vegefoods/js/jquery.easing.1.3.js"></script>
  <script src="vegefoods/js/jquery.waypoints.min.js"></script>
  <script src="vegefoods/js/jquery.stellar.min.js"></script>
  <script src="vegefoods/js/owl.carousel.min.js"></script>
  <script src="vegefoods/js/jquery.magnific-popup.min.js"></script>
  <script src="vegefoods/js/aos.js"></script>
  <script src="vegefoods/js/jquery.animateNumber.min.js"></script>
  <script src="vegefoods/js/scrollax.min.js"></script>
  <script src="vegefoods/js/main.js"></script>
  <script src="{{ URL::asset('vegefoods/js/alljs.js') }}"></script>
</body>
</html>