<!DOCTYPE html>
<html lang="en">
  <head>
    <title>BuyGiveMe</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="vegefoods/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="vegefoods/css/animate.css">
    
    <link rel="stylesheet" href="vegefoods/css/owl.carousel.min.css">
    <link rel="stylesheet" href="vegefoods/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="vegefoods/css/magnific-popup.css">

    <link rel="stylesheet" href="vegefoods/css/aos.css">

    <link rel="stylesheet" href="vegefoods/css/ionicons.min.css">

    <link rel="stylesheet" href="vegefoods/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="vegefoods/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="vegefoods/css/flaticon.css">
    <link rel="stylesheet" href="vegefoods/css/icomoon.css">
    <link rel="stylesheet" href="vegefoods/css/style.css">
  </head>
  <body class="goto-here">

    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="/home">BuyGiveMe</a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
	          <li class="nav-item"><a href="/home" class="nav-link">Home</a></li>
	          <li class="nav-item"><a href="/products" class="nav-link">Products</a></li>
	          <li class="nav-item"><a href="/categories" class="nav-link">Categories</a></li>
	          <li class="nav-item"><a href="/checkout" class="nav-link">Checkout</a></li>
	          <li class="nav-item"><a href="/about" class="nav-link">About</a></li>

	          <li class="nav-item active"><a href="/contact" class="nav-link">Contact</a></li>
	          <li class="nav-item cta cta-colored"><a href="/cart" class="nav-link"><span class="icon-shopping_cart"></span>[{{ $cart }}]</a></li>

	        </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->

    <div class="hero-wrap hero-bread" style="background-image: url('vegefoods/images/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<p class="breadcrumbs"><span class="mr-2"><a href="/home">Home</a></span> <span>Contact us</span></p>
            <h1 class="mb-0 bread">Contact us</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section contact-section bg-light">
      <div class="container">
      	<div class="row d-flex mb-5 contact-info">
          <div class="w-100"></div>
          <div class="col-md-3 d-flex">
          	<div class="info bg-white p-4">
	            <p><span>Address:</span> Fourth Street, Madina, Accra, Ghana</p>
	          </div>
          </div>
          <div class="col-md-3 d-flex">
          	<div class="info bg-white p-4">
	            <p><span>Phone:</span> <a href="tel://+233 20 435 5647">+233 (0)20 435 5647</a></p>
	          </div>
          </div>
          <div class="col-md-3 d-flex">
          	<div class="info bg-white p-4">
	            <p><span>Email:</span> <a href="mailto:info@buygiveme.com">info@buygiveme.com</a></p>
	          </div>
          </div>
          <div class="col-md-3 d-flex">
          	<div class="info bg-white p-4">
	            <p><span>Website</span> <a href="#">BUYGIVEME.COM</a></p>
	          </div>
          </div>
        </div>
        <div class="row block-9">
          <div class="col-md-6 order-md-last d-flex">
            <form action="#" class="bg-white p-5 contact-form">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Your Name">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Your Email">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Subject">
              </div>
              <div class="form-group">
                <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
              </div>
              <div class="form-group">
                <input type="submit" value="Send Message" class="btn btn-primary py-3 px-5">
              </div>
            </form>
          
          </div>

          <div class="col-md-6 d-flex">
          	<div id="map" class="bg-white"></div>
          </div>
        </div>
      </div>
    </section>

  <footer class="ftco-footer ftco-section">
  	<div class="container">
  		<div class="row">
  			<div class="mouse">
  				<a href="#" class="mouse-icon">
  					<div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
  				</a>
  			</div>
  		</div>
  		<div class="row mb-5">
  			<div class="col-md">
  				<div class="ftco-footer-widget mb-4">
  					<h2 class="ftco-heading-2">BUYGIVEME</h2>
  					<p>We are here to serve everyone. Going to the market is difficult under this hot weather. So order, we will deliver.</p>
  					<ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
  						<li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
  						<li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
  						<li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
  					</ul>
  				</div>
  			</div>
  			<div class="col-md">
  				<div class="ftco-footer-widget mb-4 ml-md-5">
  					<h2 class="ftco-heading-2">Menu</h2>
  					<ul class="list-unstyled">
  						<li><a href="/products" class="py-2 d-block">Shop</a></li>
  						<li><a href="/about" class="py-2 d-block">About</a></li>
  						<li><a href="/categories" class="py-2 d-block">Categories</a></li>
  						<li><a href="/contact" class="py-2 d-block">Contact Us</a></li>
  					</ul>
  				</div>
  			</div>
  			<div class="col-md-4">
  				<div class="ftco-footer-widget mb-4">
  					<h2 class="ftco-heading-2">Help</h2>
  					<div class="d-flex">
  						<ul class="list-unstyled mr-l-5 pr-l-3 mr-4">
  							<li><a href="/shipping" class="py-2 d-block">Shipping Information</a></li>
  							<li><a href="/returns" class="py-2 d-block">Returns &amp; Exchange</a></li>
  							<li><a href="/terms" class="py-2 d-block">Terms &amp; Conditions</a></li>
  							<li><a href="/privacy" class="py-2 d-block">Privacy Policy</a></li>
  						</ul>
  						<ul class="list-unstyled">
  							<li><a href="/faqs" class="py-2 d-block">FAQs</a></li>
  							<li><a href="/contact" class="py-2 d-block">Contact</a></li>
  						</ul>
  					</div>
  				</div>
  			</div>
  			<div class="col-md">
  				<div class="ftco-footer-widget mb-4">
  					<h2 class="ftco-heading-2">Have Questions?</h2>
  					<div class="block-23 mb-3">
  						<ul>
  							<li><span class="icon icon-map-marker"></span><span class="text">Fourth Street, Madina, Accra, Ghana</span></li>
  							<li><a href="#"><span class="icon icon-phone"></span><span class="text">+233 (0)20 435 5647</span></a></li>
  							<li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@buygiveme.com</span></a></li>
  						</ul>
  					</div>
  				</div>
  			</div>
  		</div>
  		<div class="row">
  			<div class="col-md-12 text-center">

  				<p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  					Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart color-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  				</p>
  			</div>
  		</div>
  	</div>
  </footer>

<div class="modal fade" id="viewmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Order #<span id="orderno"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="orderbox"></div>
        <form>
            @csrf
            <input type="hidden" name="id" id="id">
            <div class="form-group">
                <label for="size">Size</label>
                <select name="size" id="size" class="form-control @error('size') is-invalid @enderror" placeholder=""></select> 
            </div>
            <div class="form-group">
                <label for="qty">Quantity</label>
                <input type="number" class="form-control @error('qty') is-invalid @enderror" id="qty" placeholder="" name="qty" value="1">
            </div>
            <hr>
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary btn-lg" onclick="addtothecart()">Add to Cart</button>
        </form>
      </div>
    </div>
  </div>
</div>
  <script src="vegefoods/js/jquery.min.js"></script>
  <script src="vegefoods/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="vegefoods/js/popper.min.js"></script>
  <script src="vegefoods/js/bootstrap.min.js"></script>
  <script src="vegefoods/js/jquery.easing.1.3.js"></script>
  <script src="vegefoods/js/jquery.waypoints.min.js"></script>
  <script src="vegefoods/js/jquery.stellar.min.js"></script>
  <script src="vegefoods/js/owl.carousel.min.js"></script>
  <script src="vegefoods/js/jquery.magnific-popup.min.js"></script>
  <script src="vegefoods/js/aos.js"></script>
  <script src="vegefoods/js/jquery.animateNumber.min.js"></script>
  <script src="vegefoods/js/bootstrap-datepicker.js"></script>
  <script src="vegefoods/js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="vegefoods/js/google-map.js"></script>
  <script src="vegefoods/js/main.js"></script>
  <script src="{{ URL::asset('vegefoods/js/alljs.js') }}"></script>
  </body>
</html>