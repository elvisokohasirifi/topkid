<!DOCTYPE html>
<html lang="en">
  <head>
    <title>BuyGiveMe</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ URL::asset('vegefoods/css/open-iconic-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('vegefoods/css/animate.css') }}">
    
    <link rel="stylesheet" href="{{ URL::asset('vegefoods/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('vegefoods/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('vegefoods/css/magnific-popup.css') }}">

    <link rel="stylesheet" href="{{ URL::asset('vegefoods/css/aos.css') }}">

    <link rel="stylesheet" href="{{ URL::asset('vegefoods/css/ionicons.min.css') }}">

    <link rel="stylesheet" href="{{ URL::asset('vegefoods/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('vegefoods/css/jquery.timepicker.css') }}">

    
    <link rel="stylesheet" href="{{ URL::asset('vegefoods/css/flaticon.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('vegefoods/css/icomoon.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('vegefoods/css/style.css') }}">
  </head>
  <body class="goto-here">

    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="/home">BuyGiveMe</a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
	          <li class="nav-item"><a href="/home" class="nav-link">Home</a></li>
	          <li class="nav-item active"><a href="/products" class="nav-link">Products</a></li>
	          <li class="nav-item"><a href="/categories" class="nav-link">Categories</a></li>
	          <li class="nav-item"><a href="/checkout" class="nav-link">Checkout</a></li>
	          <li class="nav-item"><a href="/about" class="nav-link">About</a></li>

	          <li class="nav-item"><a href="/contact" class="nav-link">Contact</a></li>
	          <li class="nav-item cta cta-colored"><a href="/cart" class="nav-link"><span class="icon-shopping_cart"></span>[{{ $cart }}]</a></li>

	        </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->

    <div class="hero-wrap hero-bread" style="background-image: url('/vegefoods/images/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<p class="breadcrumbs"><span class="mr-2"><a href="/home">Home</a></span> <span class="mr-2"><a href="/home">Product</a></span> <span>{{ $product->name }}</span></p>
            <h1 class="mb-0 bread">{{ $product->name }}</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section">
    	<div class="container">
    		<div class="row">
    			<div class="col-lg-6 mb-5 ftco-animate">
    				<a href="/vegefoods/images/{{ $product->image }}" class="image-popup"><img src="/vegefoods/images/{{ $product->image }}" class="img-fluid" alt="{{ $product->name }}"></a>
    			</div>
    			<div class="col-lg-6 product-details pl-md-5 ftco-animate">
    				<h3>{{ $product->name }}</h3>
    				
    				<p class="price"><span>GHS <span id="sizeprice">{{ $sizes[0]->price }}</span></span></p>
    				<p>{{ $product->description }}
						</p>
						<div class="row mt-4">
              @if($product->has_sizes == 1)
							<div class="col-md-6">
								<div class="form-group d-flex">
		              <div class="select-wrap">
	                  <div class="icon"><span class="ion-ios-arrow-down"></span></div>
	                  <select name="" id="productprice" class="form-control" onchange="changeprice({{$sizes}})">
	                  	@foreach($sizes as $size)
                      <option value="{{ $size->sizes_id }}">{{ $size->name }}</option>
                      @endforeach
	                  </select>
	                </div>
		            </div>
							</div>
              @endif
							<div class="w-100"></div>
							<div class="input-group col-md-6 d-flex mb-3">
	             	<span class="input-group-btn mr-2">
	                	<button type="button" class="quantity-left-minus btn"  data-type="minus" data-field="">
	                   <i class="ion-ios-remove"></i>
	                	</button>
	            		</span>
	             	<input type="text" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="100">
	             	<span class="input-group-btn ml-2">
	                	<button type="button" class="quantity-right-plus btn" data-type="plus" data-field="">
	                     <i class="ion-ios-add"></i>
	                 </button>
	             	</span>
	          	</div>
	          	<div class="w-100"></div>
	          	
          	</div>
          	<p><a href="/cart" class="btn btn-black py-3 px-5">Add to Cart</a></p>
    			</div>
    		</div>
    	</div>
    </section>

    <section class="ftco-section">
    	<div class="container">
				<div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
          	<span class="subheading">Products</span>
            <h2 class="mb-4">Related Products</h2>
          </div>
        </div>   		
    	</div>
    	<div class="container">
    		<div class="row">
          @foreach($products as $product)
        <div class="col-md-6 col-lg-3 ftco-animate">
          <div class="product">
            <a href="/product/{{ $product->slug }}" class="img-prod"><img class="img-fluid" src="/vegefoods/images/{{ $product->image }}" alt="{{ $product->name }}">
              <div class="overlay"></div>
            </a>
            <div class="text py-3 pb-4 px-3 text-center">
              <h3><a href="/product/{{ $product->slug }}">{{ $product->name }}</a></h3>
              <div class="d-flex">
                <div class="pricing">
                  <p class="price"><span>GHS {{ $product->price }}</span></p>
                </div>
              </div>
              <div class="d-flex justify-content-center">
                <div class="text-center">
                  <p class="price"><button class="btn btn-primary" onclick="addtocart({{ $product->id }}, {{ $product->has_sizes }})">Add to Cart</button></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endforeach
    		</div>
    	</div>
    </section>

		<section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
      <div class="container py-4">
        <div class="row d-flex justify-content-center py-5">
          <div class="col-md-6">
          	<h2 style="font-size: 22px;" class="mb-0">Subcribe to our Newsletter</h2>
          	<span>Get e-mail updates about our latest shops and special offers</span>
          </div>
          <div class="col-md-6 d-flex align-items-center">
            <form action="#" class="subscribe-form">
              <div class="form-group d-flex">
                <input type="text" class="form-control" placeholder="Enter email address">
                <input type="submit" value="Subscribe" class="submit px-3">
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  <footer class="ftco-footer ftco-section">
  	<div class="container">
  		<div class="row">
  			<div class="mouse">
  				<a href="#" class="mouse-icon">
  					<div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
  				</a>
  			</div>
  		</div>
  		<div class="row mb-5">
  			<div class="col-md">
  				<div class="ftco-footer-widget mb-4">
  					<h2 class="ftco-heading-2">BUYGIVEME</h2>
  					<p>We are here to serve everyone. Going to the market is difficult under this hot weather. So order, we will deliver.</p>
  					<ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
  						<li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
  						<li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
  						<li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
  					</ul>
  				</div>
  			</div>
  			<div class="col-md">
  				<div class="ftco-footer-widget mb-4 ml-md-5">
  					<h2 class="ftco-heading-2">Menu</h2>
  					<ul class="list-unstyled">
  						<li><a href="/products" class="py-2 d-block">Shop</a></li>
  						<li><a href="/about" class="py-2 d-block">About</a></li>
  						<li><a href="/categories" class="py-2 d-block">Categories</a></li>
  						<li><a href="/contact" class="py-2 d-block">Contact Us</a></li>
  					</ul>
  				</div>
  			</div>
  			<div class="col-md-4">
  				<div class="ftco-footer-widget mb-4">
  					<h2 class="ftco-heading-2">Help</h2>
  					<div class="d-flex">
  						<ul class="list-unstyled mr-l-5 pr-l-3 mr-4">
  							<li><a href="/shipping" class="py-2 d-block">Shipping Information</a></li>
  							<li><a href="/returns" class="py-2 d-block">Returns &amp; Exchange</a></li>
  							<li><a href="/terms" class="py-2 d-block">Terms &amp; Conditions</a></li>
  							<li><a href="/privacy" class="py-2 d-block">Privacy Policy</a></li>
  						</ul>
  						<ul class="list-unstyled">
  							<li><a href="/faqs" class="py-2 d-block">FAQs</a></li>
  							<li><a href="/contact" class="py-2 d-block">Contact</a></li>
  						</ul>
  					</div>
  				</div>
  			</div>
  			<div class="col-md">
  				<div class="ftco-footer-widget mb-4">
  					<h2 class="ftco-heading-2">Have Questions?</h2>
  					<div class="block-23 mb-3">
  						<ul>
  							<li><span class="icon icon-map-marker"></span><span class="text">Fourth Street, Madina, Accra, Ghana</span></li>
  							<li><a href="#"><span class="icon icon-phone"></span><span class="text">+233 (0)20 435 5647</span></a></li>
  							<li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@buygiveme.com</span></a></li>
  						</ul>
  					</div>
  				</div>
  			</div>
  		</div>
  		<div class="row">
  			<div class="col-md-12 text-center">

  				<p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  					Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart color-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  				</p>
  			</div>
  		</div>
  	</div>
  </footer>
  <div class="modal fade" id="viewmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Order #<span id="orderno"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="orderbox"></div>
        <form>
            @csrf
            <input type="hidden" name="id" id="id">
            <div class="form-group">
                <label for="size">Size</label>
                <select name="size" id="size" class="form-control @error('size') is-invalid @enderror" placeholder=""></select> 
            </div>
            <div class="form-group">
                <label for="qty">Quantity</label>
                <input type="number" class="form-control @error('qty') is-invalid @enderror" id="qty" placeholder="" name="qty" value="1">
            </div>
            <hr>
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary btn-lg" onclick="addtothecart()">Add to Cart</button>
        </form>
      </div>
    </div>
  </div>
</div>

  <script src="{{ URL::asset('vegefoods/js/jquery.min.js') }}"></script>
  <script src="{{ URL::asset('vegefoods/js/jquery-migrate-3.0.1.min.js') }}"></script>
  <script src="{{ URL::asset('vegefoods/js/popper.min.js') }}"></script>
  <script src="{{ URL::asset('vegefoods/js/bootstrap.min.js') }}"></script>
  <script src="{{ URL::asset('vegefoods/js/jquery.easing.1.3.js') }}"></script>
  <script src="{{ URL::asset('vegefoods/js/jquery.waypoints.min.js') }}"></script>
  <script src="{{ URL::asset('vegefoods/js/jquery.stellar.min.js') }}"></script>
  <script src="{{ URL::asset('vegefoods/js/owl.carousel.min.js') }}"></script>
  <script src="{{ URL::asset('vegefoods/js/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ URL::asset('vegefoods/js/aos.js') }}"></script>
  <script src="{{ URL::asset('vegefoods/js/jquery.animateNumber.min.js') }}"></script>
  <script src="{{ URL::asset('vegefoods/js/bootstrap-datepicker.js') }}"></script>
  <script src="{{ URL::asset('vegefoods/js/scrollax.min.js') }}"></script>
  <script src="{{ URL::asset('vegefoods/js/main.js') }}"></script>
  <script src="{{ URL::asset('vegefoods/js/alljs.js') }}"></script>
  
  <script>
		$(document).ready(function(){

		var quantitiy=0;
		   $('.quantity-right-plus').click(function(e){
		        
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		            
		            $('#quantity').val(quantity + 1);

		          
		            // Increment
		        
		    });

		     $('.quantity-left-minus').click(function(e){
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		      
		            // Increment
		            if(quantity>0){
		            $('#quantity').val(quantity - 1);
		            }
		    });
		    
		});
	</script>
    
  </body>
</html>