<!DOCTYPE html>
<html lang="en">
  <head>
    <title>BuyGiveMe</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="vegefoods/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="vegefoods/css/animate.css">
    
    <link rel="stylesheet" href="vegefoods/css/owl.carousel.min.css">
    <link rel="stylesheet" href="vegefoods/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="vegefoods/css/magnific-popup.css">

    <link rel="stylesheet" href="vegefoods/css/aos.css">

    <link rel="stylesheet" href="vegefoods/css/ionicons.min.css">

    <link rel="stylesheet" href="vegefoods/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="vegefoods/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="vegefoods/css/flaticon.css">
    <link rel="stylesheet" href="vegefoods/css/icomoon.css">
    <link rel="stylesheet" href="vegefoods/css/style.css">
  </head>
  <body class="goto-here" id="body">

    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="/home">BuyGiveMe</a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
	          <li class="nav-item"><a href="/home" class="nav-link">Home</a></li>
	          <li class="nav-item"><a href="/products" class="nav-link">Products</a></li>
	          <li class="nav-item"><a href="/categories" class="nav-link">Categories</a></li>
	          <li class="nav-item"><a href="/about" class="nav-link">About</a></li>
	          <li class="nav-item"><a href="/contact" class="nav-link">Contact</a></li>
	          <li class="nav-item cta cta-colored"><a href="/cart" class="nav-link"><span class="icon-shopping_cart"></span>[{{ $cart }}]</a></li>
            <li class="nav-item">
              <form action="/changemarket">
                <div class="form-group">
                  <select name="" id="customercountry" class="form-control" style="padding: 0px; font-size: 13px; border: none; margin: 0px; margin-top: 7px; text-transform: uppercase; color: blue">
                        <option value="Ghana" selected="selected" style="color: blue">Madina</option>
                      </select>
                  </div>
              </form>
            </li>

	        </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->

    <div class="hero-wrap hero-bread" style="background-image: url('vegefoods/images/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<p class="breadcrumbs"><span class="mr-2"><a href="/home">Home</a></span> <span>Checkout</span></p>
            <h1 class="mb-0 bread">Checkout</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-7 ftco-animate">
            @if (session('success'))
            <div class="alert alert-success mb-4">
                {{ session('success') }}
            </div>
            @endif
            <ul style="list-style-type: none;" class="mb-4">
                 @foreach ($errors->all() as $error)
                     <li class="alert alert-danger">{{ $error }}</li>
                 @endforeach
            </ul>
            @if(!Auth::check())
            <hr>
            <p class="mb-4">Returning customer? <a href="javascript:void(0)" onclick="login()">Log in</a></p>
            <p class="mb-4">Want us to remember you? <a href="javascript:void(0)" onclick="signup()">Sign Up</a></p>
            <hr>
            @else
             <p class="mb-4">Not you? <a href="/logout">Log out</a></p>
            @endif
						<form class="billing-form">
							<h3 class="mb-4 billing-heading">Billing Details</h3>
	          	<div class="row align-items-end">
	          		<div class="col-md-12">
	                <div class="form-group">
	                	<label for="firstname">Full Name</label>
	                  <input type="text" class="form-control" placeholder="" value="{{ auth()->user()->name ?? '' }}" id="customername" required="required">
	                </div>
	              </div>
                <div class="w-100"></div>
		            <div class="col-md-12">
		            	<div class="form-group">
		            		<label for="country">Country</label>
		            		<div class="select-wrap">
		                  <div class="icon"><span class="ion-ios-arrow-down"></span></div>
		                  <select name="" id="customercountry" class="form-control">
		                  	<option value="Ghana" selected="selected">Ghana</option>
		                  </select>
		                </div>
		            	</div>
		            </div>
		            <div class="w-100"></div>
		            <div class="col-md-6">
		            	<div class="form-group">
	                	<label for="streetaddress">Street Address</label>
	                  <input type="text" class="form-control" placeholder="House number and street name" value="{{ auth()->user()->street ?? '' }}" id="customerstreet" required="required">
	                </div>
		            </div>
		            <div class="col-md-6">
		            	<div class="form-group">
	                  <input type="text" class="form-control" placeholder="Appartment, suite, unit etc: (optional)" value="{{ auth()->user()->apartment ?? '' }}" id="customerapartment">
	                </div>
		            </div>
		            <div class="w-100"></div>
		            <div class="col-md-6">
		            	<div class="form-group">
	                	<label for="towncity">Town</label>
	                  <input type="text" class="form-control" placeholder="e.g. Madina" value="{{ auth()->user()->town ?? '' }}" id="customertown" required="required">
	                </div>
		            </div>
		            <div class="w-100"></div>
		            <div class="col-md-6">
	                <div class="form-group">
	                	<label for="phone">Phone</label>
	                  <input type="tel" class="form-control" placeholder="" value="{{ auth()->user()->phone ?? '' }}" id="customerphone" required="required">
	                </div>
	              </div>
	              <div class="col-md-6">
	                <div class="form-group">
	                	<label for="email">Email Address</label>
                    @if(!Auth::check())
	                  <input type="email" class="form-control" placeholder="" value="{{ auth()->user()->email ?? '' }}" id="customeremail" name="email">
                    @else
                    <input type="email" class="form-control" placeholder="" value="{{ auth()->user()->email ?? '' }}" id="customeremail" name="email" disabled="disabled">
                    @endif
	                </div>
                </div>
                <div class="w-100"></div>
                <div class="col-md-12">
                	<div class="form-group mt-4">
                    <div>
                      <label><input type="checkbox" name="save" id="customerremember" checked="checked"> Remember my details</label>
                    </div>
									</div>
                </div>
	            </div>
					</div>
					<div class="col-xl-5">
	          <div class="row mt-5 pt-3">
	          	<div class="col-md-12 d-flex mb-5">
	          		<div class="cart-detail cart-total p-3 p-md-4">
	          			<h3 class="billing-heading mb-4">Cart Total</h3>
	          			<p class="d-flex">
		    						<span>Subtotal</span>
		    						<span>GHS {{ $totalcost }}</span>
		    					</p>
		    					<p class="d-flex">
		    						<span>Delivery</span>
		    						<span>GHS {{ $deliverycost }}</span>
		    					</p>
		    					<p class="d-flex">
		    						<span>Commission</span>
		    						<span>GHS {{ $commission }}</span>
		    					</p>
		    					<hr>
		    					<p class="d-flex total-price">
		    						<span>Total</span>
		    						<span>GHS {{ $totalcost + $commission + $deliverycost }}</span>
                    <input type="hidden" id="customeramount" value="{{ $totalcost + $commission + $deliverycost }}">
		    					</p>
								</div>
	          	</div>
	          	<div class="col-md-12">
	          		<div class="cart-detail p-3 p-md-4">
	          			<h3 class="billing-heading mb-4">Payment Method</h3>
									<div class="form-group">
										<div class="col-md-12">
											<div class="radio">
											   <label><input type="radio" value="Rave" class="mr-2" checked="checked" id="customerpayment"> Rave (Mobile Money and Card)</label>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<div class="checkbox">
											   <label><input type="checkbox" value="" class="mr-2"> I have read and I accept the terms and conditions</label>
											</div>
										</div>
									</div>
									<p><button type="button" class="btn btn-primary py-3 px-4" onclick="completeorder()">Complete Order</button></p>
								</div>
	          	</div>
	          </div>
          </div> <!-- .col-md-8 -->
        </div>
      </div>
      </form>
    </section> <!-- .section -->

		<section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
      <div class="container py-4">
        <div class="row d-flex justify-content-center py-5">
          <div class="col-md-6">
          	<h2 style="font-size: 22px;" class="mb-0">Subcribe to our Newsletter</h2>
          	<span>Get e-mail updates about our latest shops and special offers</span>
          </div>
          <div class="col-md-6 d-flex align-items-center">
            <form action="#" class="subscribe-form">
              <div class="form-group d-flex">
                <input type="text" class="form-control" placeholder="Enter email address">
                <input type="submit" value="Subscribe" class="submit px-3">
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  <footer class="ftco-footer ftco-section">
  	<div class="container">
  		<div class="row">
  			<div class="mouse">
  				<a href="#" class="mouse-icon">
  					<div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
  				</a>
  			</div>
  		</div>
  		<div class="row mb-5">
  			<div class="col-md">
  				<div class="ftco-footer-widget mb-4">
  					<h2 class="ftco-heading-2">BUYGIVEME</h2>
  					<p>We are here to serve everyone. Going to the market is difficult under this hot weather. So order, we will deliver.</p>
  					<ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
  						<li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
  						<li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
  						<li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
  					</ul>
  				</div>
  			</div>
  			<div class="col-md">
  				<div class="ftco-footer-widget mb-4 ml-md-5">
  					<h2 class="ftco-heading-2">Menu</h2>
  					<ul class="list-unstyled">
  						<li><a href="/products" class="py-2 d-block">Shop</a></li>
  						<li><a href="/about" class="py-2 d-block">About</a></li>
  						<li><a href="/categories" class="py-2 d-block">Categories</a></li>
  						<li><a href="/contact" class="py-2 d-block">Contact Us</a></li>
  					</ul>
  				</div>
  			</div>
  			<div class="col-md-4">
  				<div class="ftco-footer-widget mb-4">
  					<h2 class="ftco-heading-2">Help</h2>
  					<div class="d-flex">
  						<ul class="list-unstyled mr-l-5 pr-l-3 mr-4">
  							<li><a href="/shipping" class="py-2 d-block">Shipping Information</a></li>
  							<li><a href="/returns" class="py-2 d-block">Returns &amp; Exchange</a></li>
  							<li><a href="/terms" class="py-2 d-block">Terms &amp; Conditions</a></li>
  							<li><a href="/privacy" class="py-2 d-block">Privacy Policy</a></li>
  						</ul>
  						<ul class="list-unstyled">
  							<li><a href="/faqs" class="py-2 d-block">FAQs</a></li>
  							<li><a href="/contact" class="py-2 d-block">Contact</a></li>
  						</ul>
  					</div>
  				</div>
  			</div>
  			<div class="col-md">
  				<div class="ftco-footer-widget mb-4">
  					<h2 class="ftco-heading-2">Have Questions?</h2>
  					<div class="block-23 mb-3">
  						<ul>
  							<li><span class="icon icon-map-marker"></span><span class="text">Fourth Street, Madina, Accra, Ghana</span></li>
  							<li><a href="#"><span class="icon icon-phone"></span><span class="text">+233 (0)20 435 5647</span></a></li>
  							<li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@buygiveme.com</span></a></li>
  						</ul>
  					</div>
  				</div>
  			</div>
  		</div>
  		<div class="row">
  			<div class="col-md-12 text-center">

  				<p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  					Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart color-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  				</p>
  			</div>
  		</div>
  	</div>
  </footer>
<div class="modal fade" id="viewmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Order #<span id="orderno"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="orderbox"></div>
        <form>
            @csrf
            <input type="hidden" name="id" id="id">
            <div class="form-group">
                <label for="size">Size</label>
                <select name="size" id="size" class="form-control @error('size') is-invalid @enderror" placeholder=""></select> 
            </div>
            <div class="form-group">
                <label for="qty">Quantity</label>
                <input type="number" class="form-control @error('qty') is-invalid @enderror" id="qty" placeholder="" name="qty" value="1">
            </div>
            <hr>
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary btn-lg" onclick="addtothecart()">Add to Cart</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="loginmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="orderbox"></div>
        <form method="post" action="{{ route('login') }}">
            @csrf
            <div class="form-group">
                <label for="lemail">Email</label>
                <input id="lemail" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="lpassword">Password</label>
                <input type="password" class="form-control @error('password') is-invalid @enderror" id="lpassword" placeholder="" name="password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
            </div>
            <hr>
            <button type="submit" class="btn btn-primary">
                {{ __('Login') }}
            </button>

            @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
            @endif
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="signupmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Sign up</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="orderbox"></div>
        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Register') }}
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

  <script src="vegefoods/js/jquery.min.js"></script>
  <script src="vegefoods/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="vegefoods/js/popper.min.js"></script>
  <script src="vegefoods/js/bootstrap.min.js"></script>
  <script src="vegefoods/js/jquery.easing.1.3.js"></script>
  <script src="vegefoods/js/jquery.waypoints.min.js"></script>
  <script src="vegefoods/js/jquery.stellar.min.js"></script>
  <script src="vegefoods/js/owl.carousel.min.js"></script>
  <script src="vegefoods/js/jquery.magnific-popup.min.js"></script>
  <script src="vegefoods/js/aos.js"></script>
  <script src="vegefoods/js/jquery.animateNumber.min.js"></script>
  <script src="vegefoods/js/bootstrap-datepicker.js"></script>
  <script src="vegefoods/js/scrollax.min.js"></script>
  <script src="vegefoods/js/main.js"></script>
  <script src="vegefoods/js/sweetalert.js"></script>
  <script src="{{ URL::asset('vegefoods/js/alljs.js') }}"></script>
  <script src="https://api.ravepay.co/flwv3-pug/getpaidx/api/flwpbf-inline.js"></script>
  <script>
		$(document).ready(function(){

		var quantitiy=0;
		   $('.quantity-right-plus').click(function(e){
		        
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		            
		            $('#quantity').val(quantity + 1);

		          
		            // Increment
		        
		    });

		     $('.quantity-left-minus').click(function(e){
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		      
		            // Increment
		            if(quantity>0){
		            $('#quantity').val(quantity - 1);
		            }
		    });
		    
		});
	</script>
    
  </body>
</html>