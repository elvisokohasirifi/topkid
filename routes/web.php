<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/about', 'AboutController@index')->name('about');

Route::get('/cart', 'CartController@index')->name('cart');

Route::get('/checkout', 'CheckoutController@index')->name('checkout');

Route::get('/contact', 'ContactController@index')->name('contact');

Route::get('/product/{product}', 'ProductController@product');

Route::get('/category/{category}', 'CategoryController@category');

Route::get('/products', 'ProductController@index')->name('products');

Route::get('/categories', 'CategoryController@index')->name('categories');

Route::post('/searchproducts', 'ProductController@searchproducts');

Route::post('/addtocart0', 'CartController@add0');

Route::post('/addtocart1', 'CartController@add1');

Route::post('/addtocart2', 'CartController@add2');

Route::post('/deletecart', 'CartController@deletecart');

Route::post('/updatecart', 'CartController@updatecart');

Route::post('/completeorder', 'CheckoutController@completeorder');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout', function(){
    Session::flush();
    Auth::logout();
    return Redirect::to("/checkout");
});
