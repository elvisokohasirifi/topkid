<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string('name');
            $table->string('unit_of_measurement');
            $table->string('image');
            $table->text('description');
            $table->float('price');
            $table->text('tags');
            $table->smallInteger('categories_id');
            $table->enum('has_sizes', ['0', '1']);
            $table->enum('featured', ['0', '1']);
            $table->enum('new_price_allowed', ['0', '1']);
            $table->string('slug');
            $table->timestamps();
            $table->index('categories_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
