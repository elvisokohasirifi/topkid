<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RiderBike extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rider_bike', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->bigInteger('rider_id');
            $table->smallInteger('bike_id');
            $table->index('rider_id');
            $table->index('bike_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rider_bike');
    }
}
