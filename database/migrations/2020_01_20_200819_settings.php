<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Settings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->float('commission');
            $table->enum('commissiontype', ['0', '1']); // 0 is flat and 1 is percentage
            $table->float('delivery');
            $table->enum('deliverytype', ['0', '1']); // 0 is flat and 1 is percentage
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
